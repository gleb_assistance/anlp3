import numpy as np
import torch

from utils import char_tensor


def compute_bpc(model, string):
    """
    Given a model and a string of characters, compute bits per character
    (BPC) using that model.

    Args:
        model: RNN-based model (RNN, LSTM, GRU, etc.)
        string: string of characters

    Returns:
        BPC for that set of string.
    """

    input_tensor = char_tensor(string)
    hs = model.init_hidden()
    criterion = torch.nn.CrossEntropyLoss()
    bits = 0

    for i in range(input_tensor.shape[0]):
        p = input_tensor[i]
        x = np.zeros((1, 100))
        x[0, p] = 1
        inp = torch.tensor(x, dtype=torch.float32)

        p1 = input_tensor[i]
        x1 = np.zeros((1, 100))
        x1[0, p1] = 1
        ground_true = torch.tensor(x1, dtype=torch.float32)

        output, hs = model(inp, hs)
        loss = criterion(output, ground_true)
        bits += loss.item()

    return bits / len(string)
