import torch
import torch.nn as nn
from torch.autograd import Variable


# Here is a pseudocode to help with your LSTM implementation. 
# You can add new methods and/or change the signature (i.e., the input parameters) of the methods.
class LSTM(nn.Module):
    def __init__(self, n_chars, hidden_size, _, n_layers, embedding_size=16, batch_size=1):
        """Think about which (hyper-)parameters your model needs; i.e., parameters that determine the
        exact shape (as opposed to the architecture) of the model. There's an embedding layer, which needs 
        to know how many elements it needs to embed, and into vectors of what size. There's a recurrent layer,
        which needs to know the size of its input (coming from the embedding layer). PyTorch also makes
        it easy to create a stack of such layers in one command; the size of the stack can be given
        here. Finally, the output of the recurrent layer(s) needs to be projected again into a vector
        of a specified size."""
        super(LSTM, self).__init__()
        self.n = n_chars
        self.n_layers = n_layers
        self.batch_size = batch_size
        self.hidden_size = hidden_size

        self.hidden = self.init_hidden()

        self.lstm = nn.LSTM(input_size=n_chars, hidden_size=hidden_size,
                            num_layers=n_layers, dropout=0.1, batch_first=True)
        self.fc2 = nn.Linear(hidden_size, n_chars)

    def forward(self, input, hs):
        """Your implementation should accept input character, hidden and cell state,
        and output the next character distribution and the updated hidden and cell state."""
        x, (h, c) = self.lstm(input, hs)
        fc_in = x.view(-1, x.size(1))
        x = self.fc2(fc_in)
        return x, (h, c)

    def init_hidden(self):
        """Finally, you need to initialize the (actual) parameters of the model (the weight
        tensors) with the correct shapes."""

        hidden = (torch.zeros(self.n_layers, self.hidden_size).requires_grad_(),
                  torch.zeros(self.n_layers, self.hidden_size).requires_grad_())
        return hidden
