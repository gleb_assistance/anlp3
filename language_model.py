import random

import torch
import torch.nn as nn
import string
import time
import unidecode
import matplotlib.pyplot as plt
import numpy as np

from utils import char_tensor, random_training_set, time_since, random_chunk, CHUNK_LEN, load_dataset
from evaluation import compute_bpc
from model.model import LSTM


def generate(decoder, prime_str='A', predict_len=100, temperature=0.8):
    hidden, cell = decoder.init_hidden()
    prime_input = char_tensor(prime_str)
    # print(prime_str, prime_input)
    predicted = prime_str
    all_characters = string.printable
    # Use priming string to "build up" hidden state
    for p in range(len(prime_str) - 1):
        x = np.zeros((1, 100))
        x[0, prime_input[p]] = 1
        inp = torch.tensor(x, dtype=torch.float32)
        _, (hidden, cell) = decoder(inp, (hidden, cell))
    inp = prime_input[-1]

    for p in range(predict_len):
        x = np.zeros((1, 100))
        x[0, inp] = 1
        inp = torch.tensor(x, dtype=torch.float32)
        output, (hidden, cell) = decoder(inp, (hidden, cell))

        # Sample from the network as a multinomial distribution
        output_dist = output.data.view(-1).div(temperature).exp()
        top_i = torch.multinomial(output_dist, 1)[0]

        # Add predicted character to string and use as next input
        predicted_char = all_characters[top_i]
        predicted += predicted_char
        inp = char_tensor(predicted_char)

    return predicted


def train(decoder, decoder_optimizer, inp, target):
    #
    decoder.zero_grad()
    loss = 0
    criterion = nn.CrossEntropyLoss()
    hs = decoder.init_hidden()

    for c in range(CHUNK_LEN):
        # print(inp[c], target[c])
        x = np.zeros((1, 100))
        y = np.zeros((1, 100))
        x[0, inp[c]] = 1
        y[0, target[c]] = 1
        x = torch.tensor(x, dtype=torch.float32)
        y = torch.tensor(y)

        output, hs = decoder(x, hs)
        loss += criterion(output, target[c].view(1))
        # break
        # print(loss)

    loss.backward()
    decoder_optimizer.step()

    return loss.item() / CHUNK_LEN


def tuner(n_epochs=3000, print_every=100, plot_every=10, hidden_size=128, n_layers=2,
          lr=0.005, start_string='A', prediction_length=100, temperature=0.8, n_characters=100, generate_str=True):
    # YOUR CODE HERE
    #     TODO:
    #         1) Implement a `tuner` that wraps over the training process (i.e. part
    #            of code that is ran with `default_train` flag) where you can
    #            adjust the hyperparameters
    #         2) This tuner will be used for `custom_train`, `plot_loss`, and
    #            `diff_temp` functions, so it should also accomodate function needed by
    #            those function (e.g. returning trained model to compute BPC and
    #            losses for plotting purpose).
        decoder = LSTM(n_characters, hidden_size, n_characters, n_layers)
        decoder_optimizer = torch.optim.Adam(decoder.parameters(), lr=lr)
        start = time.time()
        all_losses = []
        loss_avg = 0

        for epoch in range(1, n_epochs + 1):
            loss = train(decoder, decoder_optimizer, *random_training_set())
            loss_avg += loss

            if generate_str and epoch % print_every == 0:
                print('[{} ({} {}%) {:.4f}]'.format(time_since(start), epoch, epoch / n_epochs * 100, loss))
                print(generate(decoder, start_string, prediction_length, temperature=temperature), '\n')
            if epoch % plot_every == 0:
                all_losses.append(loss_avg / plot_every)
                loss_avg = 0
        return decoder, all_losses


def plot_loss(lr_list):
    # YOUR CODE HERE
    #     TODO:
    #         1) Using `tuner()` function, train X models where X is len(lr_list),
    #         and plot the training loss of each model on the same graph.
    #         2) Don't forget to add an entry for each experiment to the legend of the graph.
    #         Each graph should contain no more than 10 experiments.
    loss_list = []

    for lr in lr_list:
        model, loss = tuner(lr=lr)
        loss_list.append(loss)
        print(f"model: {lr}")
        print(compute_bpc(model, string))
    for lr, loss in zip(lr_list, loss_list):
        plt.plot(loss, label=f"lr: {lr}")
    plt.legend(loc="best")
    plt.title("loss function for different learning rate values")
    plt.show()


def diff_temp(temp_list):
    #     TODO:
    #         1) Using `tuner()` function, try to generate strings by using different temperature
    #         from `temp_list`.
    #         2) In order to do this, create chunks from the test set (with 200 characters length)
    #         and take first 10 characters of a randomly chosen chunk as a priming string.
    #         3) What happen with the output when you increase or decrease the temperature?

    file = load_dataset('./data/dickens_test.txt')
    start_index = random.randint(0, len(file) - CHUNK_LEN - 1)
    end_index = start_index + CHUNK_LEN + 1

    test_text = file[start_index:end_index]
    print("--- ORIGINAL TEXT ---")
    print(test_text)

    print("\n---\n")
    test_prefix = test_text[:10]
    for temp in temp_list:
        print(f"model temp: {temp}")
        model, loss = tuner(temperature=temp, n_epochs=3000, generate_str=False)
        generated_text = generate(model, prime_str=test_prefix, predict_len=190, temperature=temp)
        print(test_prefix + generated_text)
        print("\n---\n")


def custom_train(hyperparam_list):
    """
    Train model with X different set of hyperparameters, where X is 
    len(hyperparam_list).

    Args:
        hyperparam_list: list of dict of hyperparameter settings

    Returns:
        bpc_dict: dict of bpc score for each set of hyperparameters.
    """
    TEST_PATH = './data/dickens_test.txt'
    string = unidecode.unidecode(open(TEST_PATH, 'r').read())
    bpc_dict = {}
    for i, model_params in enumerate(hyperparam_list):
        model, _ = tuner(**model_params)
        bpc = compute_bpc(model, string)
        print(f"Model {i}, params: {model_params}\nbpc: {np.round(bpc, 3)}")
        bpc_dict[f"{i}"] = bpc

    return bpc_dict