## Course: Advanced Natural Language Processing

### Assignment 3: Character-level Language Modelling with LSTM


1. **default train loss**

<img src="./imgs/default_train_loss.png" height="350">


2. **Hyperparameter Tuning**

    i. Model 0, params: {'hidden_size': 128, 'n_layers': 2}

    bpc: 6.005

    ii. Model 1, params: {'hidden_size': 128, 'n_layers': 3}

    bpc: 5.514

    iii. Model 2, params: {'hidden_size': 256, 'n_layers': 3}
    
    bpc: 4.558



3. **Plotting the Training Losses**

<img src="./imgs/plot_loss2.png" height="350">


4. **Generating at different temperatures**

We took a random 200 character chunk from the dickens_test.txt file. We then trained a model to generate 190 characters
based on the first 10 characters from the selected chunk. A low temperature models tends to generate a shorter words and
they do not adapt to the context well. However, high temperature models generate advanced words with a non-existing words
and grammar uses that results as unreadable patchwork text.

```text

--- ORIGINAL TEXT ---
egrets.  'Oh, Trot,' I seemed to hear my aunt say once more;
and I understood her better now - 'Blind, blind, blind!'
We both kept silence for some minutes.  When I raised my eyes, Ifound that she wa
---
model temp: 0.1
egrets.  'I was the same the stands the could the fired the said the stance the saber the stard the starkle the stard the starked to the state and see and said the said the did the said her said her s
---
model temp: 0.3
egrets.  'I was door said in the been the come that had be in the dould still have no day that was the streat the could last the streen that it was could part the down to have have have been the diste
---
model temp: 0.5
egrets.  'Now with the fail the was have stime of lents and to be and was a sand of all said to be a last, and a see be to that it in the had to the stranged of mreass the didard me and gate to her ma
---
model temp: 0.7
egrets.  'How as he was his so pacress to there
farning of a slool before, will windon. But were dowled, when the said sobter than strused boot staghed by the sworened without of made mare in the ste
---
model temp: 0.8
egrets.  'That whose, andmore and 
what of my beble were, I were a forded night walkle; as I can his wonding and so ratences and while to her seeple the quinged, and my many become at ather so men, a
---
model temp: 0.9
egrets.  'Iw Ihe thented?  'As I eam at it
was latteranterelce frite of her have homanes and 'mucain, 'I mostYaughes.  I dreard thanshe tempraking strocty momention. found that cansing as I thind the
```
