import argparse

import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import unidecode
import string
import time

from utils import char_tensor, random_training_set, time_since, CHUNK_LEN
from language_model import plot_loss, diff_temp, custom_train, train, generate
from model.model import LSTM


def main():
    parser = argparse.ArgumentParser(
        description='Train LSTM'
    )

    parser.add_argument(
        '--default_train', dest='default_train',
        help='Train LSTM with default hyperparameter',
        action='store_true'
    )

    parser.add_argument(
        '--custom_train', dest='custom_train',
        help='Train LSTM while tuning hyperparameter',
        action='store_true'
    )

    parser.add_argument(
        '--plot_loss', dest='plot_loss',
        help='Plot losses chart with different learning rates',
        action='store_true'
    )

    parser.add_argument(
        '--diff_temp', dest='diff_temp',
        help='Generate strings by using different temperature',
        action='store_true'
    )

    args = parser.parse_args()

    all_characters = string.printable
    n_characters = len(all_characters)

    int2char = dict(enumerate(all_characters))
    char2int = {ch: i for i, ch in int2char.items()}
    if False or args.default_train:
        n_epochs = 3000
        print_every = 10
        plot_every = 50
        hidden_size = 128
        n_layers = 2

        lr = 0.005
        decoder = LSTM(n_characters, hidden_size, n_characters, n_layers)
        decoder_optimizer = torch.optim.Adam(decoder.parameters(), lr=lr)

        start = time.time()
        all_losses = []
        loss_avg = 0

        for epoch in range(1, n_epochs+1):
            loss = train(decoder, decoder_optimizer, *random_training_set())
            loss_avg += loss

            if epoch % print_every == 0:
                print('[{} ({} {}%) {:.4f}]'.format(time_since(start), epoch, epoch/n_epochs * 100, loss))
                print(generate(decoder, 'A', 100), '\n')

            if epoch % plot_every == 0:
                all_losses.append(loss_avg / plot_every)
                loss_avg = 0
        plt.plot(all_losses)
        plt.show()

    if True or args.custom_train:
        hyperparam_list = [{"hidden_size": 128, "n_layers": 2},
                           {"hidden_size": 128, "n_layers": 3},
                           {"hidden_size": 256, "n_layers": 3}]
        ########################################################################
        bpc = custom_train(hyperparam_list)

        for keys, values in bpc.items():
            print("model {} BPC: {}".format(keys, values))

    if False or args.plot_loss:
        lr_list = [0.0005, 0.001, 0.007, 0.005, 0.002, 0.01]
        plot_loss(lr_list)

    if False or args.diff_temp:
        temp_list = [0.1, 0.3, 0.5, 0.7, 0.8, 0.9]
        diff_temp(temp_list)


if __name__ == "__main__":
    main()
